<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/main.css">
    <link rel="icon" type="image/png" href="img/ALEPH_LOGO_NEGRO-03.png" />
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
 <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>



<link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@1,300&display=swap" rel="stylesheet">
    <title>Examen Data Science</title>
</head>
<body>
<div class="alert alert-danger" role="alert"  id="alert" style="display:none">
 Debes ingresar tu nombre!
</div>
<div class="alert alert-danger" role="alert"  id="alert2" style="display:none">
 El nombre ya se registro
</div>
<div id="admin">
    <a href="forms/login.php">Admin</a>
</div>
    <div id="container">

        <div id="form"> 
            
       
        <section id="data">
        
        <div id="form"> 
           
            <form action="php/add.php" method="POST">
            <img src="img/aleph.png">
  <div class="form-group">
  <center><label for="text"><strong>"Examen Diagnóstico"</strong></label></center><br>
    <label for="text"><strong>Nombre completo del aplicante</strong></label>
    <input type="text" class="form-control" name="name" id="name">
  </div>
 
 
  <center>
  <button type="submit" class="btn btn-info" id="btn">Comenzar prueba</button>
</center>
</form>
        </section>

        </div>
    </div>
    
</body>
</html>
<script>
 window.location.hash="no-back-button";
window.location.hash="Again-No-back-button";
window.onhashchange=function(){window.location.hash="no-back-button";}
    $(function(){
        $('#btn').click(function(e){
           
            var name=$('#name').val();
            if(name=="" || name==" "){
                e.preventDefault(); 
                $('#alert').css("display","block", function(){
                    $('#alert').hide("3000");
                });
                console.log("accediendo");
            }else{
                console.log("accediendo");
                $('#alert2').load("php/comprobar.php",{
                    name:name,
                    function(){
                        $('#alert2').css("display","block");
                    }
                });
               
            }
        });
    });
</script>