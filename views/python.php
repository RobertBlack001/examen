<?php 
session_start();
require "../conexion/conexion.php";
date_default_timezone_set('America/Mexico_City');
$id=$_SESSION['id'];
$cont=1;
$_SESSION['ff_r']=date("H:i:s");
$_SESSION['fi_python']=date("H:i:s");
$ff_r=$_SESSION['ff_r'];
$fi_r=$_SESSION['fi_r'];
$inicio_r=new DateTime($fi_r);
$fin_r=new DateTime($ff_r);
$dateInterval = $inicio_r->diff($fin_r);
$time_r= $dateInterval->format('%H:%i:%s').PHP_EOL;
if(!isset($_SESSION['tiempo_r'])){
    $_SESSION['tiempo_r']=$time_r;
}
$sql ="select * from pregunta where area='Python'";
$result =mysqli_query($connect,$sql);

?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/main.css">
    <link rel="icon" type="image/png" href="../img/ALEPH_LOGO_NEGRO-03.png" />
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
 <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
<link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@1,300&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@600&display=swap" rel="stylesheet">

<scrpt src="https://code.jquery.com/jquery-3.5.1.min.js">
    <title>Preguntas</title>
</head>
<body>
<header>
<nav>
           <img src="../img/ALEPH_LOGO_NEGRO-03.png" style="width:45px; heigth:45px;">
        <label>   Examen diagnóstico Data Science</label>
       
       </nav>
</header>
       <div id="preguntas">
       

                 <section id="pre">
                
               <label style="font-size: 18px;"><strong>Sección de Lenguaje de Programación Python</strong></label>
               <hr>
                        
                <form action="../php/sesiones/r_p.php" method="POST">
               
                    
                    <?php while($row=$result->fetch_assoc()){?>
                   
                        <label>  
                      <?php echo utf8_encode($row['pregunta']);?></label>
                      <img src="<?php echo $row['img']?>" id="img">
                      
                      <textarea class="form-control" rows="2" id="comment" name="resp_p[]"><?php if(isset($_SESSION['resp_p'])){ $array=$_SESSION['resp_p']; echo $array[$cont-1]; }?></textarea>

                      
                    <?php $cont=$cont+1; }?>
                    <button type="submit" class="btn btn-outline-secondary float-right" id="btn">Siguiente</button></a>
                    </form>
                    
                   <a href="r.php"> <button  class="btn btn-outline-secondary float-right" id="btn">Anterior</button></a>
                 </section>
             </div>
             <footer id="foot"></footer>
</body>
</html>
<script>

    $(function(){
        var img=$('#img');
        $(img).on("error", function(event){
            $(event.target).css("display","none");
        });
    });
</script>