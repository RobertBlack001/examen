-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 27-08-2020 a las 18:15:48
-- Versión del servidor: 10.4.13-MariaDB
-- Versión de PHP: 7.3.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `examen_dc`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `aplicante`
--

CREATE TABLE `aplicante` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) COLLATE utf8mb4_spanish_ci NOT NULL,
  `fecha` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `calificacion_aplicante`
--

CREATE TABLE `calificacion_aplicante` (
  `calificacion` tinyint(4) NOT NULL,
  `id_aplicante` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pregunta`
--

CREATE TABLE `pregunta` (
  `id` int(11) NOT NULL,
  `pregunta` varchar(500) COLLATE utf8mb4_spanish_ci NOT NULL,
  `area` varchar(50) COLLATE utf8mb4_spanish_ci NOT NULL,
  `img` varchar(50) COLLATE utf8mb4_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `pregunta`
--

INSERT INTO `pregunta` (`id`, `pregunta`, `area`, `img`) VALUES
(1, '1. Describe que es una variable y los distintos tipos de variables cuando hablamos de estadística.', 'Estadistica', ''),
(2, '2. En tus propias palabras, ¿Qué es una muestra?,¿Qué tipos de selección conoces?, y ¿Bajo qué tipo de escenarios utilizamos muestras?', 'Estadistica', ''),
(3, '3. Explica las medidas de tendencia central, y ¿Cuál de estas medidas cambia si un solo valor en los datos cambia', 'Estadistica', ''),
(4, '4. Cierto o Falso. Una curva de distribucion normal es simétrica en el cero, y la suma del área debajo de la curva es 1. Justifica tu respuesta.', 'Estadistica', ''),
(5, '5. Explica la diferencia entre un parámetro y un estimador.', 'Estadistica', ''),
(6, '6. En tus propias palabras, ¿Qué es un coeficiente de correlación?', 'Estadistica', ''),
(7, '7. ¿Cuál es la diferencia entre causalidad y correlación?', 'Estadistica', ''),
(8, '8. ¿Cuál es la relación entre nivel de significancia y nivel de confianza?', 'Estadistica', ''),
(9, '9. Diferencia entre Frecuentista y Bayesiano', 'Estadistica', ''),
(10, '10. ¿Qué es el valor p?(p-value)', 'Estadistica', ''),
(11, '1. Describe la diferencia entre los siguientes objetos: Matrix, Data Frame y List', 'R', ''),
(12, '2. En tus propias palabras, explica la diferencia entre R y RStudio', 'R', ''),
(13, '3. Observa la siguiente data frame y escribe el código para obtener la media de \"speed\", pero sólo para las observaciones en que \"dist\" sea mayor o igual a 20 y menor o igual a 40', 'R', '../img/r_3.png'),
(14, '4. Escribe el código para exportar el data frame anterior a csv. Vuelve a considerar la regla de: sólo las observaciones en que \"dist\" sea mayor o igual a 20 y menor o igual a 40. Bonus: Códifica el csv en \"UTF-8\"', 'R', ''),
(15, '5. Considera el siguiente data frame. Escribe el código para: 1. Obtener el recuento de observaciones de cada valor en \"Species\"(frecuencia).\r\n2. Obtener el promerio de \"Sepal.length\" para cada valor de \"Species\"(Hint:Puedes iterar sobre cada valor úico de \"Species\"). Bounus:Utiliza dplyr para encontrar la solución anterior, o itera utiizando alguna funcion de la familia apply(apply,sapply,lapply)', 'R', '../img/r_5.png'),
(16, '6. Enlista las librerías/paquetes de R que utilizas o has utilizado', 'R', ''),
(17, '7. En tus propias palabras, ¿Qué es un factor en R, y en qué escenarios es recomendable usarlos?', 'R', ''),
(18, '1. Enlista los modulos que has utilizado', 'Python', ''),
(19, '2. Describe la diferencia entre lista, tupla y diccionario', 'Python', ''),
(20, '3. Describe la diferencia entre una lista, un numpy array y un pandas.series', 'Python', ''),
(21, '4. Explica la diferencia entre una propiedad y un método', 'Python', ''),
(22, '5. Observa el siguiente código y describe lo que hace', 'Python', '../img/p_5.png'),
(23, '6. Observa el siguiente list. Escribe el código para guardar en la lista de \"hot_days\" sólo los elementos de la lista \"july_temperatures\" que sean mayores a 90', 'Python', '../img/p_6.png'),
(24, '7. Observa detenidamente y con cuidado la sig. imagen. \"articles\" es un diccionario de tamaño desconocido que a su vez contiene otros diccionarios como el que se muestra en la imagen. Describe el list comprehension utilizado y describe el uso de la funcion \"f-strings\". Explica el resultado', 'Python', '../img/p_7.png'),
(25, '1. Escribe los diferentes motores de bases de datos que has utilizado/utilizas', 'SQL', ''),
(26, '2. ¿Por qué no debes olvidar el WHERE en el DELETE FROM', 'SQL', ''),
(27, '3. Explica las diferencias entre inner join, left join y outter join', 'SQL', ''),
(28, '4. ¿Qué es una llave primaria?', 'SQL', ''),
(29, '5. En tus propias palabras, ¿Qué es un query?', 'SQL', ''),
(30, '1. Describe la cuenta root', 'Linux', ''),
(31, '2. ¿Qué es GUI', 'Linux', ''),
(32, '3. ¿Qué hace el comando pwd?', 'Linux', ''),
(33, '4. ¿Qué es un Daemon?', 'Linux', ''),
(34, '5. ¿Qué es un comando grep?', 'Linux', ''),
(35, '1. Diferencia entre método GET y POST', 'WEB', ''),
(36, '2. Lenguajes relacionados al desarrollo web que conoces, usas, has escuchado', 'WEB', ''),
(37, '3. Describe el proceso por el cuál construyes un sitio web', 'WEB', ''),
(38, '4. Cómo tomas en cuenta SEO, UX y seguridad cuando desarrollas un sitio web', 'WEB', ''),
(39, 'En tus palabras, cuál es la diferencia entre un sitio estático y un sitio dinámico', 'WEB', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `respuestas`
--

CREATE TABLE `respuestas` (
  `id` int(11) NOT NULL,
  `respuesta` text COLLATE utf8mb4_spanish_ci NOT NULL,
  `id_aplicante` int(11) NOT NULL,
  `id_pregunta` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tiempo`
--

CREATE TABLE `tiempo` (
  `id` int(11) NOT NULL,
  `id_aplicante` int(11) NOT NULL,
  `area` varchar(50) COLLATE utf8mb4_spanish_ci NOT NULL,
  `tiempo` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `id` int(11) NOT NULL,
  `usuario` varchar(50) COLLATE utf8mb4_spanish_ci NOT NULL,
  `password` varchar(20) COLLATE utf8mb4_spanish_ci NOT NULL,
  `tipo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id`, `usuario`, `password`, `tipo`) VALUES
(2, 'admin', 'admin2020', 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `aplicante`
--
ALTER TABLE `aplicante`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nombre` (`nombre`);

--
-- Indices de la tabla `pregunta`
--
ALTER TABLE `pregunta`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `respuestas`
--
ALTER TABLE `respuestas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tiempo`
--
ALTER TABLE `tiempo`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `aplicante`
--
ALTER TABLE `aplicante`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `pregunta`
--
ALTER TABLE `pregunta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT de la tabla `respuestas`
--
ALTER TABLE `respuestas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tiempo`
--
ALTER TABLE `tiempo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
