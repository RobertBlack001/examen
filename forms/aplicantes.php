<?php 
require "../conexion/conexion.php";
session_start();

if(isset($_SESSION['administrador'])){
 

$sql ="select * from aplicante";
$result =mysqli_query($connect,$sql);

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/main.css">
    <link rel="icon" type="image/png" href="../img/ALEPH_LOGO_NEGRO-03.png" />
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
 <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
<link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@1,300&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@600&display=swap" rel="stylesheet">
    <title>Aplicantes</title>
</head>
<body>
<header>
<nav>
           <img src="../img/ALEPH_LOGO_NEGRO-03.png" style="width:45px; heigth:45px;">
        <label>   Aplicantes</label>
       
       </nav>
</header>
    <div id="container_apli">
        <section id="aplicantes">
        <table class="table table-striped">
  <thead>
    <tr>
      
      <th scope="col">Aplicante</th>
      <th scope="col">Fecha</th>
      <th><a href="../php/logout.php"><button type="button"  class="btn btn-outline-secondary">Salir</button></a></th>
      
    </tr>
  </thead>
  <tbody>
  <?php while($row=$result->fetch_assoc()){?>
    <tr>
     
      <td><?php echo $row['nombre']?></td>
      <td><?php echo $row['fecha']?></td>
      <td>
          <a href="../php/respuestas.php?clAplicante=<?php echo $row['id'];?>"><button type="button" class="btn btn-dark">Respuestas</button></a>
      </td>
    </tr>
  <?php }?>
  </tbody>
</table>
        </section>
    </div>


</body>
</html>
<?php 

 } else{

header('location:login.php');
}
?>