<?php 

require "../conexion/conexion.php";
$sql ="select * from pregunta";
$result =mysqli_query($connect,$sql);

?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/main.css">
    <link rel="icon" type="image/png" href="../img/aleph.png" />
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
 <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
<link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@1,300&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@600&display=swap" rel="stylesheet">

<scrpt src="https://code.jquery.com/jquery-3.5.1.min.js">
    <title>Preguntas</title>
</head>
<body>
   
<header>
<nav>
           <img src="../img/aleph.png" style="width:80px; heigth:80px;">
        <label>   Examen diagnostico Data Science</label>
       
       </nav>
</header>

       <div id="preguntas">
       

                 <section id="pre">
                 <section id="instrucciones">
               
               <strong>
                        <p>
                            Instrucciones: <br>
                            Responde a tantas preguntas como te sea posible. Te recomendamos 
                            tratar de responder sólo aquellas en que te sientas seguro para responder.
                             Sin embargo, recuerda que es una competencia, por lo tanto, trata de
                             responder la mayor cantidad de preguntas que te sea posible.
                        </p>
                       </strong>
               </section>
                        
                <form action="../php/register.php" method="POST">
               
                    
                    <?php while($row=$result->fetch_assoc()){?>
                   
                        <label>  
                      <?php echo $row['area']." ".$row['pregunta'];?></label>
                      <img src="<?php echo $row['img']?>" id="img">
                      
                      <textarea class="form-control" rows="2" id="comment" name="resp[]"></textarea>

                      
                    <?php }?>
                 

<center>

  <button type="submit" class="btn btn-info" id="btn">Enviar</button>
</center>
                    </form>
                 </section>
             </div>
             <footer id="foot"></footer>
</body>
</html>
<script>

    $(function(){
        var img=$('#img');
        $(img).on("error", function(event){
            $(event.target).css("display","none");
        });
    });
</script>