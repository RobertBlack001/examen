<?php 
require "../../conexion/conexion.php";
session_start();
if(isset($_SESSION['administrador'])){
$clAplicante=$_GET['clAplicante'];
$aplicante="select * from aplicante where id='$clAplicante'";
$result3=mysqli_query($connect,$aplicante);
$imagen="../";
$cal;
$sql="select pregunta,area,respuesta,nombre,img
from respuestas,aplicante,pregunta
where aplicante.id=id_aplicante && pregunta.id=id_pregunta && id_aplicante=$clAplicante && area='Estadistica'";
$sql2="select pregunta,area,respuesta,nombre,img
from respuestas,aplicante,pregunta
where aplicante.id=id_aplicante && pregunta.id=id_pregunta && id_aplicante=$clAplicante && area='R'";
$sql3="select pregunta,area,respuesta,nombre,img
from respuestas,aplicante,pregunta
where aplicante.id=id_aplicante && pregunta.id=id_pregunta && id_aplicante=$clAplicante && area='Python'";
$sql4="select pregunta,area,respuesta,nombre,img
from respuestas,aplicante,pregunta
where aplicante.id=id_aplicante && pregunta.id=id_pregunta && id_aplicante=$clAplicante && area='Linux'";
$sql5="select pregunta,area,respuesta,nombre,img
from respuestas,aplicante,pregunta
where aplicante.id=id_aplicante && pregunta.id=id_pregunta && id_aplicante=$clAplicante && area='WEB'";
$sql6="select pregunta,area,respuesta,nombre,img
from respuestas,aplicante,pregunta
where aplicante.id=id_aplicante && pregunta.id=id_pregunta && id_aplicante=$clAplicante && area='SQL'";
$tiempo="select aplicante.id, area, tiempo from aplicante, tiempo 
where id_aplicante=aplicante.id && id_aplicante=$clAplicante";
$calificacion=$connect->query("select * from calificacion_aplicante where id_aplicante=$clAplicante");
//$cal=mysqli_query($connect,$calificacion);
if($calificacion->num_rows==0){
  $cal="No calificado";
}else{
  while($fila=$calificacion->fetch_row()){
    $cal=$fila[0];
  }
}
$result2=mysqli_query($connect,$tiempo);
$result=mysqli_query($connect,$sql);
$r=mysqli_query($connect,$sql2);
$python=mysqli_query($connect,$sql3);
$linux=mysqli_query($connect,$sql4);
$web=mysqli_query($connect,$sql5);
$sql=mysqli_query($connect,$sql6);

?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../../css/main.css">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
 <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
<link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@1,300&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@600&display=swap" rel="stylesheet">
    <title>Respuestas</title>
    <style type="text/css">
#reporte {
   
    margin-left:20px;
   
    width: 190mm;
}

#portada{
  margin-top:100px;
  height:1000px;
}
#l{
  
  width: 155px;
  height:50px;
  align-items:center;
}
#logo2{
  width: 400px;
  height:410px;
}
#logo1{
  width:60px;
  height:60px;
}
#titulo{
  text-align:center;
}

#aplicante{
  width: 155px;
  height:50px;
}
#rp{

 
  margin-right:25px;
  margin-left:25px;
  width:500px;
  height:300px;
}
#content{
  width:680px;
  height:50px;
}
    </style>
</head>
<body>

    <div id="reporte">
        <table id="portada">
          <tr>
       <td id="l"></td>
          <td id="l"><img src="../../img/aleph.png" id="logo2">
         
        </td>
        </tr>
        <tr>
          <td></td>
          <td id="titulo"><h4><strong>"Examen diagnóstico Data Science"</strong></h4></td>
        </tr>
        <tr>
        <td id="aplicante"></td>
          <td id="aplicante"><label><h4><strong>Nombre del aplicante:</strong></h4> <?php while($nombre_aplicante=$result3->fetch_assoc()){  echo $nombre_aplicante['nombre']?>
                 </label></td>
          </tr>
          <tr>
          <td id="aplicante"></td>
                  <td><label><h4><strong>Fecha de Aplicación:</strong></h4>  <?php echo $nombre_aplicante['fecha']?></label></td>
                  <?php }?>
                </tr>
                <tr>
          <td id="aplicante"></td>
                  <td><label><h4><strong>Calificación de examen:</strong></h4><?php echo $cal ?></label></td>
                  
                </tr>
            
              
                <tr> <td id="aplicante"></td></tr>
                <tr> <td id="aplicante"></td></tr>
        
      </table>

         <div class="row">
                <table id="rp">
                <tr>
                <td id="content"> <label><h2>Respuestas</h2></label>
                </td></tr>
                <tr>
                <td id="content"> <label><h3>Estadística <hr></h3></label>
                </td>
                </tr>
                <?php while($row=$result->fetch_assoc()){?>
                <tr>
                <td id="content"> <strong>  <label id="pregunta">  
                      <?php echo utf8_encode($row['pregunta'])?></label></strong></td>
                </tr>
                <tr>
                <td id="content"><label  id="respuesta"> R= <?php echo $row['respuesta']?></label></td>
                </tr>
                <?php }?>
                  <tr>
                <td id="content"> <label><h3>Lenguaje R <hr></h3></label>
                </td>
                </tr>
                <?php while($row=$r->fetch_assoc()){?>
                <tr>
                <td id="content"> <strong>  <label id="pregunta">  
                      <?php echo utf8_encode($row['pregunta'])?></label></strong></td>
                      </tr>
                      <tr>
                    <td>  <img src="<?php echo $imagen.$row['img'];?>" id="img"></td>
                </tr>
                <tr>
                <td id="content"><label  id="respuesta"> R= <?php echo $row['respuesta']?></label></td>
                </tr>
                <?php }?>

                <tr>
                <td id="content"> <label><h3>Lenguaje Python <hr></h3></label>
                </td>
                </tr>
                <?php while($row=$python->fetch_assoc()){?>
                <tr>
                <td id="content"> <strong>  <label id="pregunta">  
                      <?php echo utf8_encode($row['pregunta'])?></label></strong></td>
                      </tr>
                      <tr>
                    <td>  <img src="<?php echo $imagen.$row['img'];?>" id="img"></td>
                </tr>
                <tr>
                <td id="content"><label  id="respuesta"> R= <?php echo $row['respuesta']?></label></td>
                </tr>
                <?php }?>
                
                <tr>
                <td id="content"> <label><h3>Lenguaje SQL <hr></h3></label>
                </td>
                </tr>
                <?php while($row=$sql->fetch_assoc()){?>
                <tr>
                <td id="content"> <strong>  <label id="pregunta">  
                      <?php echo utf8_encode($row['pregunta'])?></label></strong></td>
                </tr>
                <tr>
                <td id="content"><label  id="respuesta"> R= <?php echo $row['respuesta']?></label></td>
                </tr>
                <?php }?>

                <tr>
                <td id="content"> <label><h3>Linux <hr></h3></label>
                </td>
                </tr>
                <?php while($row=$linux->fetch_assoc()){?>
                <tr>
                <td id="content"> <strong>  <label id="pregunta">  
                      <?php echo utf8_encode($row['pregunta'])?></label></strong></td>
                </tr>
                <tr>
                <td id="content"><label  id="respuesta"> R= <?php echo $row['respuesta']?></label></td>
                </tr>
                <?php }?>

                <tr>
                <td id="content"> <label><h3>WEB <hr></h3></label>
                </td>
                </tr>
                <?php while($row=$web->fetch_assoc()){?>
                <tr>
                <td id="content"> <strong>  <label id="pregunta">  
                      <?php echo utf8_encode($row['pregunta'])?></label></strong></td>
                </tr>
                <tr>
                <td id="content"><label  id="respuesta"> R= <?php echo $row['respuesta']?></label></td>
                </tr>
                <?php }?>
                </table>
          
                    
      
         </div>
 
                 </div>
                      
</body>

</html>

<script>

    $(function(){
        var img=$('#img');
        $(img).on("error", function(event){
            $(event.target).css("display","none");
        });


    });
</script>
   
<?php 

 } else{

header('location:../forms/login.php');
}
?>