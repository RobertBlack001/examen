<?php 
require "../conexion/conexion.php";
//header("Location: ../forms/modal.php ");
session_start();
if(isset($_SESSION['administrador'])){
$clAplicante=$_GET['clAplicante'];
$_SESSION['clAplicante']=$clAplicante;
$aplicante="select * from aplicante where id='$clAplicante'";
$result3=mysqli_query($connect,$aplicante);

$sql="select pregunta,area,respuesta,nombre,img
from respuestas,aplicante,pregunta
where aplicante.id=id_aplicante && pregunta.id=id_pregunta && id_aplicante=$clAplicante && area='Estadistica'";
$sql2="select pregunta,area,respuesta,nombre,img
from respuestas,aplicante,pregunta
where aplicante.id=id_aplicante && pregunta.id=id_pregunta && id_aplicante=$clAplicante && area='R'";
$sql3="select pregunta,area,respuesta,nombre,img
from respuestas,aplicante,pregunta
where aplicante.id=id_aplicante && pregunta.id=id_pregunta && id_aplicante=$clAplicante && area='Python'";
$sql4="select pregunta,area,respuesta,nombre,img
from respuestas,aplicante,pregunta
where aplicante.id=id_aplicante && pregunta.id=id_pregunta && id_aplicante=$clAplicante && area='Linux'";
$sql5="select pregunta,area,respuesta,nombre,img
from respuestas,aplicante,pregunta
where aplicante.id=id_aplicante && pregunta.id=id_pregunta && id_aplicante=$clAplicante && area='WEB'";
$sql6="select pregunta,area,respuesta,nombre,img
from respuestas,aplicante,pregunta
where aplicante.id=id_aplicante && pregunta.id=id_pregunta && id_aplicante=$clAplicante && area='SQL'";
$tiempo="select aplicante.id, area, tiempo from aplicante, tiempo 
where id_aplicante=aplicante.id && id_aplicante=$clAplicante";
$calificacion=$connect->query("select * from calificacion_aplicante where id_aplicante=$clAplicante");
//$cal=mysqli_query($connect,$calificacion);
if($calificacion->num_rows==0){
  $cal="No calificado";
}else{
  while($fila=$calificacion->fetch_row()){
    $cal=$fila[0];
  }
}
$result2=mysqli_query($connect,$tiempo);
$result=mysqli_query($connect,$sql);
$r=mysqli_query($connect,$sql2);
$python=mysqli_query($connect,$sql3);
$linux=mysqli_query($connect,$sql4);
$web=mysqli_query($connect,$sql5);
$sql=mysqli_query($connect,$sql6);

?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/main.css">
    <link rel="icon" type="image/png" href="../img/ALEPH_LOGO_NEGRO-03.png" />
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
 <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
<link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@1,300&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@600&display=swap" rel="stylesheet">

<scrpt src="https://code.jquery.com/jquery-3.5.1.min.js">
    <title>Respuestas</title>
</head>
<body>
   
<header>
<nav>
           <img src="../img/ALEPH_LOGO_NEGRO-03.png" style="width:45px; heigth:45px;">
        <label>  Respuestas examen diagnostico Data Science </label>
       
       </nav>
</header>
<div class="modal fade" id="miModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
      <label>Asignar calificación</label>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="myModalLabel"></h4>
			</div>
			<div class="modal-body">
      <form action="calificacion.php" method="post">
  <div class="form-group row">
    <label  class="col-sm-2 col-form-label">Calificación:</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" placeholder="0-10" name="calificacion" id="cal">
    </div>
    <div class="col-sm-4"></div>
    <div class="col-sm-4 pull-left">
    <button type="submit" class="btn btn-secondary">Aceptar</button></div>
    
  </div>
</form>
			</div>
		</div>
	</div>
</div>
       <div id="preguntas">
          

                 <section id="pre">
                 <div class="row">
                 <div class="col-md-3" ><label><h5>Nombre del aplicante: <?php while($nombre_aplicante=$result3->fetch_assoc()){  echo $nombre_aplicante['nombre']?></h5></label>
                  <?php }?> </div>
                  <div class="col-md-3"><button type="button" class="btn btn-outline-secondary" data-toggle="modal" data-target="#miModal">
	Calificar
</button></div>
                  <div class="col-md-3"><form action="reporte/index.php?clAplicante=<?php echo $clAplicante ?>" method="post"> <button type="submit" class="btn btn-outline-secondary" name="crear">Reporte</button></form></div>
                  <div class="col-md-3"><a href="../forms/aplicantes.php"><button type="button" class="btn btn-outline-secondary">Inicio</button></a></div>
               </div>
              
               <div class="row">
               <div class="col-md-3">Calificación: <?php echo $cal ?></div>
               </div>
                     <label>Tiempo por Sección:</label>
                 <table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">Estadística</th>
      <th scope="col">R</th>
      <th scope="col">Python</th>
      <th scope="col">SQL</th>
      <th scope="col">Linux</th>
      <th scope="col">WEB</th>
    </tr>
  </thead>
  <tbody>
    <tr>
    <?php while($time=$result2->fetch_assoc()){?>
      <td><?php echo $time['tiempo']?></td>
      
      <?php }?>
    </tr>
                    </table>

                    <label><h3>Estadística</h3></label>
                    <hr>
                    <?php while($row=$result->fetch_assoc()){?>
                   
                        <label>  
                      <?php echo utf8_encode($row['pregunta'])?></label>
                      <img src="<?php echo $row['img']?>" id="img">
                      <br>
                      <label style="color:green"> R= <?php echo $row['respuesta']?></label><br>
                      
                    <?php }?>
                    <label><h3>Lenguaje R</h3></label>
                    <hr>
                    <?php while($row=$r->fetch_assoc()){?>
                   
                        <label>  
                      <?php echo utf8_encode($row['pregunta'])?></label>
                      <img src="<?php echo $row['img']?>" id="img">
                      <br>
                      <label style="color:green"> R= <?php echo $row['respuesta']?></label><br>
                      

                      
                    <?php }?>
                    <label><h3>Lenguaje Python</h3></label>
                    <hr>
                    <?php while($row=$python->fetch_assoc()){?>
                   
                        <label>  
                      <?php echo utf8_encode($row['pregunta'])?></label>
                      <img src="<?php echo $row['img']?>" id="img">
                      <br>
                      <label style="color:green"> R= <?php echo $row['respuesta']?></label><br>
                      

                      
                    <?php }?>
                    <label><h3>Lenguaje SQL</h3></label>
                    <hr>
                    <?php while($row=$sql->fetch_assoc()){?>
                   
                        <label>  
                      <?php echo utf8_encode($row['pregunta'])?></label>
                      <img src="<?php echo $row['img']?>" id="img">
                      <br>
                      <label style="color:green"> R= <?php echo $row['respuesta']?></label><br>
                      

                      
                    <?php }?>
                    <label><h3>Linux</h3></label>
                    <hr>
                    <?php while($row=$linux->fetch_assoc()){?>
                   
                        <label>  
                      <?php echo utf8_encode($row['pregunta'])?></label>
                      <img src="<?php echo $row['img']?>" id="img">
                      <br>
                      <label style="color:green"> R= <?php echo $row['respuesta']?></label><br>
                      

                      
                    <?php }?>
                    <label><h3>WEB</h3></label>
                    <hr>
                    <?php while($row=$web->fetch_assoc()){?>
                   
                        <label>  
                      <?php echo utf8_encode($row['pregunta'])?></label>
                      <img src="<?php echo $row['img']?>" id="img">
                      <br>
                      <label style="color:green"> R= <?php echo $row['respuesta']?></label><br>
                      

                      
                    <?php }?>
                 
                 </section>
             </div>
             <footer id="foot"></footer>
</body>
</html>
<script>

    $(function(){
        var img=$('#img');
        $(img).on("error", function(event){
            $(event.target).css("display","none");
        });


    });
</script>
<?php 

 } else{

header('location:../forms/login.php');
}
?>