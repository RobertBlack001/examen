<?php 
session_start();
mb_internal_encoding('UTF-8');
mb_http_output('UTF-8');
require "../conexion/conexion.php";
date_default_timezone_set('America/Mexico_City');
$id=$_SESSION['id'];
$_SESSION['resp_w']=$_POST['resp_w'];

$_SESSION['ff_web']=date("H:i:s");
$ff_web=$_SESSION['ff_web'];
$fi_web=$_SESSION['fi_web'];
$inicio_web=new DateTime($fi_web);
$fin_web=new DateTime($ff_web);

$dateInterval = $inicio_web->diff($fin_web);
$time_w= $dateInterval->format('%H:%i:%s').PHP_EOL;
if(!isset($_SESSION['tiempo_web'])){
   $_SESSION['tiempo_web']=$time_w;
}
// Secciones
$secciones=array('Estadística','R','Python','SQL','Linux','WEB');

// Obtencion de tiempo por seccion
$tiempo_1=$_SESSION['tiempo_estadistica'];
$tiempo_2=$_SESSION['tiempo_r'];
$tiempo_3=$_SESSION['tiempo_python'];
$tiempo_4=$_SESSION['tiempo_sql'];
$tiempo_5=$_SESSION['tiempo_linux'];
$tiempo_6=$_SESSION['tiempo_web'];
$tiempo_seccion=array($tiempo_1,$tiempo_2,$tiempo_3,$tiempo_4,$tiempo_5,$tiempo_6);

// Consultas para agregar tiempo por seccion
for($i=0; $i<=6-1;$i++){
 
   $insertar_tiempo=("insert into tiempo (id,id_aplicante,area,tiempo)
    values('null','$id','$secciones[$i]','$tiempo_seccion[$i]')");
   $time1=$ejecutar=mysqli_query($connect,$insertar_tiempo);
}

$respuestas_estadistica=$_SESSION['resp_e'];
$respuestas_r=$_SESSION['resp_r'];
$respuestas_python=$_SESSION['resp_p'];
$respuestas_sql=$_SESSION['resp_sql'];
$respuestas_linux=$_SESSION['resp_l'];
$respuestas_web=$_SESSION['resp_w'];



$respuestas=array_merge($respuestas_estadistica,$respuestas_r,$respuestas_python,$respuestas_sql,$respuestas_linux,$respuestas_web);

foreach($respuestas as $llave => $data) {
   $question=$llave+1;
   if(empty($data)){
      $data="No contesto";
    }
   $consulta="INSERT INTO respuestas (id,respuesta,id_aplicante,id_pregunta)
   VALUES('null','$data','$id','$question')";
      $result=mysqli_query($connect,$consulta);
}
header("Location:../index.php");
session_destroy();
?>